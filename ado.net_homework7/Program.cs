﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ado.net_homework7.DataAccessLevel;

namespace ado.net_homework7
{
    class Program
    {
        static void Main(string[] args)
        {
            #region initial database
            //Visitor visitor = new Visitor
            //{
            //    FullName = "test",
            //    InDate = DateTime.Now,
            //    OutDate = DateTime.Now,
            //    IIN = "000000000000",
            //    VisitPurpose = "test"
            //};

            //using (var context = new VisitorContext())
            //{
            //    context.Visitors.Add(visitor);
            //    context.SaveChanges();

            //} 
            #endregion

            VisitorServices visitorServices = new VisitorServices();
            while (true)
            {
                string menu = "";
                Console.WriteLine();
                Console.WriteLine("***************************************");
                Console.WriteLine("Enter 1 to check in visiter");
                Console.WriteLine("Enter 2 to check out visiter");
                Console.WriteLine("Enter 3 to view all visiters");
                Console.WriteLine("Enter 0 to quit");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        using (var context = new VisitorContext())
                        {
                            context.Visitors.Add(visitorServices.CheckInVisitor());
                            context.SaveChanges();
                        }
                        break;

                    case "2":
                        visitorServices.CheckOutVisitor();
                        break;
                    case "3":
                        visitorServices.GetAllVisitors();
                        break;
                    case "0":
                        System.Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ado.net_homework7.Models;
using System.Text.RegularExpressions;
using ado.net_homework7.DataAccessLevel;

namespace ado.net_homework7
{
    public class VisitorServices
    {
        public Visitor CheckInVisitor()
        {
            Visitor visitor = new Visitor();
            while (true)
            {
                Console.WriteLine("Enter Name of Visiter");
                visitor.FullName = Console.ReadLine().ToLower();
                if (!Regex.IsMatch(visitor.FullName, @"^[\p{L}]+$"))
                {
                    Console.WriteLine("Name must contains only letters");
                }
                else break;
            }

            while (true)
            {
                Console.WriteLine("Enter Visiter IIN");
                visitor.IIN = Console.ReadLine().ToLower();
                if (!Regex.IsMatch(visitor.IIN, @"^\d{12}(?:\d{2})?$"))
                {
                    Console.WriteLine("Name must contains 12 digits");
                }
                else break;
            }

            Console.WriteLine("Enter visit purpose");
            visitor.VisitPurpose = Console.ReadLine().ToLower();
            visitor.InDate = DateTime.Now;
            return visitor;
        }

        public void CheckOutVisitor()
        {
            string IIN = "";
            using (var context = new VisitorContext())
            {
                while (true)
                {
                    Console.WriteLine("Enter Visiter IIN");
                    IIN = Console.ReadLine().ToLower();
                    if (!Regex.IsMatch(IIN, @"^\d{12}(?:\d{2})?$"))
                    {
                        Console.WriteLine("Name must contains 12 digits");
                    }
                    else break;
                }

                var visitors = context.Visitors.ToList();
                foreach (var item in visitors)
                {
                    if (item.IIN == IIN)
                    {
                        item.OutDate = DateTime.Now;
                        Console.WriteLine($"Visitor check out date is {item.OutDate}");
                        context.SaveChanges();
                        return;
                    }

                }
                Console.WriteLine("Visitor not exist");
            }

        }

        public void GetAllVisitors()
        {
            using (var context = new VisitorContext())
            {
                var visitors = context.Visitors.ToList();
                foreach (var item in visitors)
                {
                    Console.WriteLine();
                    Console.WriteLine("----------------------------------------------------");
                    Console.WriteLine($"Visitor name is {item.FullName}");
                    Console.WriteLine($"Visitor IIN is {item.IIN}");
                    Console.WriteLine($"Visitor check in date {item.InDate}");
                    Console.WriteLine($"Visitor check out date {item.OutDate}");
                }
            }
        }
    }
}

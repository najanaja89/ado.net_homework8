﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework7.Models
{
    public class Visitor
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public DateTime InDate { get; set; }
        public DateTime OutDate { get; set; }
        public string IIN { get; set; }
        public string VisitPurpose { get; set; }

        public Visitor()
        {
            Id = Guid.NewGuid();
        }
    }
}

namespace ado.net_homework7.DataAccessLevel
{
    using ado.net_homework7.Models;
    using System;
    using System.Data.Entity;
    using System.Linq;


    public class VisitorContext : DbContext
    {
        // Your context has been configured to use a 'VisitorContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ado.net_homework7.DataAccessLevel.VisitorContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'VisitorContext' 
        // connection string in the application configuration file.
        public VisitorContext()
            : base("name=VisitorContext")
        {
        }

        public DbSet<Visitor> Visitors { get; set; }
        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}